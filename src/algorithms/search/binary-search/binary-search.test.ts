import binarySearch from '@code/algorithms/search/binary-search/binary-search';

describe('binary-search', () => {
  test('returns target value index in odd-length array', () => {
    const arr = [1, 2, 5, 8, 9];
    expect(binarySearch(arr, 8)).toEqual(3);
  });
  test('returns target value index in even-length array', () => {
    const arr = [1, 2, 5, 8, 9, 10];
    expect(binarySearch(arr, 8)).toEqual(3);
  });
  test('returns -1 for missing value', () => {
    const arr = [1, 2, 5, 8, 9];
    expect(binarySearch(arr, 3)).toEqual(-1);
  });
});
