function binarySearch(
  arr: number[],
  target: number,
  lo: number = 0,
  hi: number = arr.length - 1
): number {
  while (lo <= hi) {
    const mid = Math.floor((lo + hi) / 2);
    if (arr[mid] === target) {
      return mid;
    } else if (arr[mid] < target) {
      lo = mid + 1;
    } else {
      hi = mid - 1;
    }
  }
  return -1;
}

export default binarySearch;
