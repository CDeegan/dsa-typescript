import linearSearch from '@code/algorithms/search/linear-search/linear-search';

describe('linear-search', () => {
  test('returns target value index', () => {
    const arr = [1, 5, 2, 8, 5];
    expect(linearSearch(arr, 8)).toEqual(3);
  });
  test('returns -1 for missing value', () => {
    const arr = [1, 5, 2, 8, 5];
    expect(linearSearch(arr, 3)).toEqual(-1);
  });
});
