import bubbleSort from '@code/algorithms/sort/bubble-sort/bubble-sort';

test('bubble-sort', () => {
  const arr = [9, 3, 7, 4, 72, 333, 42];
  expect(bubbleSort(arr)).toEqual([3, 4, 7, 9, 42, 72, 333]);
});
