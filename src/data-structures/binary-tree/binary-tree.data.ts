import BinaryTree from './binary-tree';

const treeA = new BinaryTree(20);
treeA.root.right = {
  value: 50,
  right: {
    value: 100,
    right: null,
    left: null
  },
  left: {
    value: 30,
    right: {
      value: 45,
      right: null,
      left: null
    },
    left: {
      value: 29,
      right: null,
      left: null
    }
  }
};

treeA.root.left = {
  value: 10,
  right: {
    value: 15,
    right: null,
    left: null
  },
  left: {
    value: 5,
    right: {
      value: 7,
      right: null,
      left: null
    },
    left: null
  }
};

const treeB = new BinaryTree(20);

treeB.root.right = {
  value: 50,
  right: null,
  left: {
    value: 30,
    right: {
      value: 45,
      right: {
        value: 49,
        left: null,
        right: null
      },
      left: null
    },
    left: {
      value: 29,
      right: null,
      left: {
        value: 21,
        right: null,
        left: null
      }
    }
  }
};

treeB.root.left = {
  value: 10,
  right: {
    value: 15,
    right: null,
    left: null
  },
  left: {
    value: 5,
    right: {
      value: 7,
      right: null,
      left: null
    },
    left: null
  }
};

export { treeA, treeB };
