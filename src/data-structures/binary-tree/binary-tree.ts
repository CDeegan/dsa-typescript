class BinaryNode<T> {
  value: T;
  left: BinaryNode<T> | null;
  right: BinaryNode<T> | null;

  constructor(value: T) {
    this.value = value;
    this.left = this.right = null;
  }
}

class BinaryTree<T> {
  root: BinaryNode<T>;

  constructor(value: T) {
    this.root = new BinaryNode(value);
  }

  preOrderTraversal(): T[] {
    return this.preOrderVisit(this.root);
  }

  private preOrderVisit(node: BinaryNode<T> | null, path: T[] = []): T[] {
    if (node) {
      path.push(node.value);
      this.preOrderVisit(node.left, path);
      this.preOrderVisit(node.right, path);
    }
    return path;
  }

  postOrderTraversal(): T[] {
    return this.postOrderVisit(this.root);
  }

  private postOrderVisit(node: BinaryNode<T> | null, path: T[] = []): T[] {
    if (node) {
      this.postOrderVisit(node.left, path);
      this.postOrderVisit(node.right, path);
      path.push(node.value);
    }
    return path;
  }

  inOrderTraversal(): T[] {
    return this.inOrderVisit(this.root);
  }

  private inOrderVisit(node: BinaryNode<T> | null, path: T[] = []): T[] {
    if (node) {
      this.inOrderVisit(node.left, path);
      path.push(node.value);
      this.inOrderVisit(node.right, path);
    }
    return path;
  }

  breadthFirstSearch(target: T): boolean {
    const queue: (BinaryNode<T> | null)[] = [this.root];

    while (queue.length > 0) {
      const curr = queue.shift();

      if (!curr) {
        continue;
      }

      if (curr.value === target) {
        return true;
      }

      queue.push(curr.left);
      queue.push(curr.right);
    }
    return false;
  }

  isEqual(tree: BinaryTree<T>): boolean {
    return this.compareNode(this.root, tree.root);
  }

  private compareNode(a: BinaryNode<T> | null, b: BinaryNode<T> | null): boolean {
    if (a === null && b === null) {
      return true;
    }

    if (a === null || b === null) {
      return false;
    }

    if (a.value !== b.value) {
      return false;
    }

    return this.compareNode(a.left, b.left) && this.compareNode(a.right, b.right);
  }
}

export default BinaryTree;
