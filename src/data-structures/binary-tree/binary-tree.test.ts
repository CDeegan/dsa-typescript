import { treeA, treeB } from './binary-tree.data';

describe('binary-tree', () => {
  test('pre-order traversal', () => {
    expect(treeA.preOrderTraversal()).toEqual([20, 10, 5, 7, 15, 50, 30, 29, 45, 100]);
  });

  test('post-order traversal', () => {
    expect(treeA.postOrderTraversal()).toEqual([7, 5, 15, 10, 29, 45, 30, 100, 50, 20]);
  });

  test('in-order traversal', () => {
    expect(treeA.inOrderTraversal()).toEqual([5, 7, 10, 15, 20, 29, 30, 45, 50, 100]);
  });

  test('BFS returns true for existing value', () => {
    expect(treeA.breadthFirstSearch(45)).toEqual(true);
  });

  test('BFS returns false for missing value', () => {
    expect(treeA.breadthFirstSearch(78)).toEqual(false);
  });

  test('isEqual returns true for matching trees', () => {
    expect(treeA.isEqual(treeA)).toEqual(true);
  });

  test('isEqual returns false for non-matching trees', () => {
    expect(treeA.isEqual(treeB)).toEqual(false);
  });
});
